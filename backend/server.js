import  express  from "express";
import userRoutes from "./app/routes/userRoutes.js";
import hospitalRoutes from "./app/routes/hospitalRoutes.js";
import attendantRoutes from './app/routes/attendantRoutes.js';
import contactRoutes from "./app/routes/contactRoutes.js";
import patientRoutes from "./app/routes/patientRoutes.js";
// import config from './config/config.json' assert { type: "json" };
import Sequelize from 'sequelize';
const app = express();
const port = 5000;
import cors  from 'cors';

app.use(express.static('Public'))

app.use(cors({
     origin: ["http://localhost:3000"],
     methods: ['GET', 'POST', 'PUT', 'DELETE'],
     credentials: true
}))

// app.use(cors())


import sequelize from "./config/sequelize.js";


sequelize.sync()
.then(result=>{console.log(result)})
.catch(error => {console.log(error)})


// // Initialize Sequelize with the configuration
const connectToPostgres = () => {
 sequelize.authenticate().then(()=>{
  console.log('Connection has been established successfully.');
 }).catch((error) =>{
  console.log('Unable to connect to the database:', error);
  process.exit(1);
 });
    return sequelize;
}

// const sequelize = new Sequelize(config.development);

// Connect to PostgreSQL
connectToPostgres();
// console.log(sequelize);
app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.use(express.json());


app.use("/api/contact/", contactRoutes);
app.use("/api/user/", userRoutes);
app.use("/api/hospital/", hospitalRoutes);
app.use("/api/attendant/", attendantRoutes);
app.use("/api/patient/", patientRoutes);;


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
