import { DataTypes } from 'sequelize';
import  sequelize  from '../../config/sequelize.js';

const Patient = sequelize.define('patients', {
  patient_name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  patient_age:{

    type:DataTypes.INTEGER,
    allowNull: true
  },
  patient_gender:{
    type:DataTypes.STRING,
    allowNull: false,

  },
  patient_phone:{

    type:DataTypes.STRING,
    allowNull: false
  },
  patient_address:{
    type:DataTypes.STRING,
    allowNull: false
  },

  patient_state:{
    type:DataTypes.INTEGER,
    allowNull: false,
    

  },
  patient_city:{
    type:DataTypes.INTEGER,
    allowNull: false,

  },
  patient_pincode:{
    type:DataTypes.INTEGER,
    allowNull: false

  },
  
 });

//Patient.belongsTo(User, { foreignKey: 'user_id' }); // Define the user relationship
//Hospital.belongsToMany(Doctor, { through: 'doctors_hospitals', foreignKey: 'hospital_id', otherKey: 'doctor_id' }); // Define the many-to-many relationship with Doctor

export default Patient;
