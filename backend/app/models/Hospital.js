import { DataTypes } from 'sequelize';
import  sequelize  from '../../config/sequelize.js';
import User from './User.js';

const Hospital = sequelize.define('hospitals', {

  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  logo:{

    type:DataTypes.BLOB('long'),
    allowNull: true
  },
  email:{
    type:DataTypes.STRING,
    allowNull: false,
    // unique: true

  },
  phone_primary:{

    type:DataTypes.STRING,
    allowNull: false
  },
  phone_secondary:{
    type:DataTypes.STRING,
    allowNull: true

  },
  address:{
    type:DataTypes.STRING,
    allowNull: false


  },
  // state_id:{
  //   type:DataTypes.INTEGER
   
    

  // },
  // city_id:{
  //   type:DataTypes.INTEGER
   

  // },
  pincode:{
    type:DataTypes.INTEGER,
    allowNull: false

  },
  website:{
    type:DataTypes.STRING,
    allowNull: false
},
password:{
  type:DataTypes.STRING,
  allowNull: false

},
conpassword:{
  type:DataTypes.STRING,
  allowNull: false

},
  user_id:{
    type:DataTypes.INTEGER,
    allowNull: false,
    unique: true,
    references: {
      model: 'users',
      key: 'id'
    },
    onDelete: 'cascade'

  },
//  }, {
  // tableName: 'hospitals', // Set the table name explicitly
  // timestamps: true, // Add timestamps automatically
  // underscored: true, // Use underscored naming convention for columns
});

Hospital.belongsTo(User, { foreignKey: 'user_id' }); // Define the user relationship
//Hospital.belongsToMany(Doctor, { through: 'doctors_hospitals', foreignKey: 'hospital_id', otherKey: 'doctor_id' }); // Define the many-to-many relationship with Doctor

export default Hospital;
