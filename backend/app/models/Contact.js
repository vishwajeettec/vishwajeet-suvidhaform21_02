import { DataTypes } from 'sequelize';
import  sequelize  from '../../config/sequelize.js';

const Contact = sequelize.define('contacts', {

  name: {
    type:DataTypes.STRING,
    allowNull: true
  },
  email:{

    type:DataTypes.STRING,
    allowNull: true
  },
  subject:{
    type:DataTypes.STRING,
    allowNull: false,
  

  },
  message:{

    type:DataTypes.STRING,
    allowNull: false
  },
 
  
});

export default Contact;
