import { DataType } from "sequelize";
import sequelize from "../../config/sequelize";
import City from "./City";

const State = sequelize.define("States", {
    state_name: {
        type: DataType.STRING,
        allowNull: false
    }
});

State.hasMany(City);

export default State;
