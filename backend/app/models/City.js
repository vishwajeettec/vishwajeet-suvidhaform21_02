import { DataType } from "sequelize";

import sequelize from "../../config/sequelize";
import State from "./State";

const City = sequelize.define('City',{
    city_name: {
        type: DataType.STRING,
        allowNull: false
    }
});

City.belongsTo(State);

export default City;