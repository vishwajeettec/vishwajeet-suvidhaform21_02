import { DataTypes } from 'sequelize';
import  sequelize  from '../../config/sequelize.js';
import Patient from './Patient.js';

const Attendant = sequelize.define('attendants', {


  attendant_name:{

    type:DataTypes.STRING,
    allowNull: true
  },
  address:{
    type:DataTypes.STRING,
    allowNull: false,
  

  },
  occupation:{

    type:DataTypes.STRING,
    allowNull: false
  },
  telephone:{
    type:DataTypes.INTEGER,
    allowNull: true

  },
  relation_with_patient:{
    type:DataTypes.STRING,
    allowNull: false


  },
  signature:{
    type:DataTypes.STRING,
    allowNull: false

  },
  
});

Attendant.belongsTo(Patient, { foreignKey: 'patient_id' }); // Define the user relationship

export default Attendant;
