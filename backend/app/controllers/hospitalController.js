import User from "../models/User.js";
import Hospital from "../models/Hospital.js";
import { body, validationResult } from "express-validator";
import bcrypt from "bcrypt";
import multer from "multer";

// const storage = multer.memoryStorage();
// const upload = multer({ storage: storage });

export const validateHospital = [
  body("name").notEmpty().withMessage("Name is required"),
  body("email").isEmail().withMessage("Invalid email"),
  body("password")
    .isLength({ min: 6 })
    .withMessage("Password must be at least 6 characters long"),
  // Add more validation rules as needed
];

export class hospitalController {
  static async addHospital(req, res) {
    try {
      if (!req.body.email) {
        return res
          .status(400)
          .json({ Status: false, error: "Email is required" });
      }

      // Check if the email is already registered
      const existingUser = await User.findOne({
        where: { email: req.body.email },
      });
      if (existingUser) {
        return res.status(400).json({ error: "Email is already registered" });
      }

      // Hash the password and conpassword using bcrypt
      const hashedPassword = await bcrypt.hash(req.body.password, 10);
      const hashedConPassword = await bcrypt.hash(req.body.conpassword, 10);

      // Create a new user instance
      const user = await User.create({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword,
        userType: "hospital",
      });

      // Create a new hospital instance
      const hospital = await Hospital.create({
        name: req.body.name,
        logo: req.file.filename,
        // logo: req.file.buffer, // Store image buffer
        email: req.body.email,
        phone_primary: req.body.phone_primary,
        phone_secondary: req.body.phone_secondary,
        address: req.body.address,
        pincode: req.body.pincode,
        website: req.body.website,
        password: hashedPassword,
        conpassword: hashedConPassword,
        user_id: user.id, // Associate the hospital with the user
      });

      // // Return the success response
      return res.status(200).json({ Status: true, msg: "Registered successfully", hospital, user });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        Status: false,
        error: "Error during registration",
        status: "Query Error",
      });
    }
  }

  static async getHospital(req, res) {
    try {
      // Import the Hospital model
      const hospitals = await Hospital.findAll();

      // Return the retrieved hospitals as JSON response
      return res.status(200).json({ hospitals });
    } catch (error) {
      console.error(error);
      // Handle error response
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }

  // get hospital details with the help of id
  static async getHospitalById(req, res) {
    try {
      // Import the Hospital model
      const hospital = await Hospital.findByPk(req.params.id);

      // Return the retrieved hospital as JSON response
      return res.status(200).json({ hospital });
    } catch (error) {
      console.error(error);
      // Handle error response
      return res.status(500).json({ error: "hospital not found " });
    }
  }
}

export default hospitalController;
