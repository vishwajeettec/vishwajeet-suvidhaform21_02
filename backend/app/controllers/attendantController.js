import Attendant from '../models/Attendant.js';

export class attendantController {
  
   
static async addAttendant(req, res) {
 
    try {

             // Check if the email is already registered
             const existingUser = await Attendant.findOne({ where: { email: req.body.email } });
             if (existingUser) {
                 return res.status(400).json({ error: 'Email is already registered' });
             }

        // Create a new hospital instance
        const attendant = await Attendant.create({
            attendant_name: req.body.attendant_name,
            address: req.body.address,
            occupation: req.body.occupation,
            telephone: req.body.telephone,
            relation_with_patient: req.body.relation_with_patient,
            signature: req.body.signature,
        });

        // Return the success response
        return res.status(200).json({ Status: true, msg: 'Registered successfully', attendant });
    } catch (error) {
        console.error(error);
        return res.status(500).json({ Status: false, error: 'Error during registration', status: 'Query Error' });
    }
}

  
     // get attendant with the help of id

     static async getAttendantById(req, res) {
        try {
            // Import the Hospital model
            const attendant = await Attendant.findByPk(req.params.id);
            
            // Return the retrieved hospital as JSON response
            return res.status(200).json({ attendant });
        } catch (error) {
            console.error(error);
            // Handle error response
            return res.status(500).json({ error: 'attendant not found '});
        }
    }







  

}


export default attendantController;