import State from '../models/State.js'; // Importing the State model

class CityController {
    // Function to get cities by state ID
    static async getCitiesByState(req, res) {
        const { state_id } = req.params; // Extract state_id from request parameters

        try {
            const state = await State.findById(state_id); // Find the state by ID
            if (!state) {
                return res.status(404).json({ message: 'State not found' });
            }
            
            const cities = state.cities; // Get the cities associated with the state
            res.json(cities); // Send the cities as JSON response
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal Server Error' }); // Handle errors
        }
    }
}

export default CityController;
