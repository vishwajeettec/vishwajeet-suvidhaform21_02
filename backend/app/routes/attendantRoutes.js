import attendantController from '../controllers/attendantController.js';
import express from 'express';
const attendantRoutes = express.Router();


// Registration route
attendantRoutes.post('/register', attendantController.addAttendant); // Change to UserController

attendantRoutes.get('/:id', attendantController.getAttendantById);



 export default attendantRoutes;