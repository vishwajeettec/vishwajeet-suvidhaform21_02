import StateController from "../controllers/stateController";
import express from 'express';
const stateRoutes = express.Router();

// Get all states

stateRoutes.get('/', StateController.getStates);

