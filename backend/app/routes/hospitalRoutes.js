import hospitalController from "../controllers/hospitalController.js";
import express from 'express';
const hospitalRoutes = express.Router();

import multer from "multer";
import path from "path";

const storage = multer.memoryStorage();


// Image Upload using Multer

// const storage = multer.diskStorage({
//     destination: (req, file, cb) => {
//         cb(null, 'Public/Images')
//     },
//     filename: (req, file, cb) => {
//         cb(null, file.fieldname + "_" + Date.now() + path.extname(file.originalname))
//         // cb(null, Date.now() + '-' + file.originalname);
//     }
// })
const upload = multer({
    storage: storage
})



// Add hospital
hospitalRoutes.post('/addHospital', upload.single('logo'), hospitalController.addHospital);

// Get all hospitals
hospitalRoutes.get('/', hospitalController.getHospital);

// get the hospital with the particular id

hospitalRoutes.get('/:id', hospitalController.getHospitalById);

export default hospitalRoutes;