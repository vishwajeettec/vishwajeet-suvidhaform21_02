import CityController from "../controllers/cityController";
import express from 'express';

const router = express.Router();

// get api for getting all the cities based in the state id


router.get('/getcitiesByState/:state_id', CityController.getCitiesByState);

export default router;
