import userController from '../controllers/userController.js';
import express from 'express';
const userRoutes = express.Router();

// Login route
userRoutes.post('/login', userController.login); // Change to UserController

// Registration route
userRoutes.post('/register', userController.registration); // Change to UserController


 export default userRoutes;