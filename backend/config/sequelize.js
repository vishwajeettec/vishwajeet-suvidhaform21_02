// sequelize.mjs

import Sequelize from 'sequelize';

const sequelize = new Sequelize('postgres://postgres:Pass@12345@localhost:5432/hospital');

const testConnection = async() => {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}
testConnection();

export default sequelize; 
