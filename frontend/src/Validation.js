// validation.js

// Function to validate name field
export const validateName = (name, touched, dirty) => {
    if (!touched && !dirty) return true; // Skip validation if field hasn't been touched or modified
    return /^[a-zA-Z\s]{2,}$/.test(name.trim());
  };
  
  // Function to validate email field
  export const validateEmail = (email, touched, dirty) => {
    if (!touched && !dirty) return true;
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email.trim());
  };
  
  // Function to validate phone number field
  export const validatePhone = (phone1, touched, dirty) => {
    if (!touched && !dirty) return true;
    return /^\d{10}$/.test(phone1.trim());
  };
  // Function to validate phone number field
  export const validateSecPhone = (phone2, touched, dirty) => {
    if (!touched && !dirty) return true;
    return /^\d{10}$/.test(phone2.trim());
  };
  
  // Function to validate pincode field
  export const validatePincode = (pincode, touched, dirty) => {
    if (!touched && !dirty) return true;
    return /^\d{6}$/.test(pincode.trim());
  };
  
  // Function to validate website URL field
  // export const validateWebsite = (website, touched, dirty) => {
  //   if (!touched && !dirty) return true;
  //   return /^(https?:\/\/)?([\da-z.-]+)\.([a-z.]{2,6})([/\w.-]*)*\/?$/.test(website.trim());
  // };
  export const validateWeb = (website, touched, dirty) => {
    if (!touched && !dirty) return true; // Skip validation if field hasn't been touched or modified
    return /^[a-zA-Z0-9]{1,20}$/.test(website.trim());
};
  // Function to validate address field
  export const validateAddress = (address, touched, dirty) => {
    if (!touched && !dirty) return true;
    return address.trim().length > 0;
  };
  
  // Function to validate address field
  export const validateCity = (city, touched, dirty) => {
    if (!touched && !dirty) return true;
    return city.trim().length > 0;
  };
  
  // Function to validate address field
  export const validateState = (state, touched, dirty) => {
    if (!touched && !dirty) return true;
    return state.trim().length > 0;
  };
  
  // Function to validate password field
  export const validatePassword = (password, touched, dirty) => {
    if (!touched && !dirty) return true;
    // Password should have minimum 8 characters, at least one uppercase letter,
    // one lowercase letter, one digit, and one special character
    const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$/;
    return passwordRegex.test(password);
  };
  
  export const validateCnfPassword = (cnfPassword, touched, dirty) => {
    if (!touched && !dirty) return true;
    // Password should have minimum 8 characters, at least one uppercase letter,
    // one lowercase letter, one digit, and one special character
    const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$/;
    return passwordRegex.test(cnfPassword);
  };

 
  export const validatePasswordMatch = (password, cnfPassword, touched, dirty) => {
    if (!touched && !dirty) return true;

    // Check if both fields are non-empty and match
    return password.trim() === cnfPassword.trim();
};