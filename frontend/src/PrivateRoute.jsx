import React from 'react'
import {  Outlet, Navigate } from 'react-router-dom';

const PrivateRoute = () => {
let  logedIn=true;
   // privateroute
   if(logedIn){
    return (
      <div>
        <Outlet />
      </div>
    )
   }else{
    return (
      <Navigate to="/login" />
    )
   }
}

export default PrivateRoute