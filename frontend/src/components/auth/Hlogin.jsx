import React, { useState } from 'react';
import './Auth.css';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { validateEmail, validatePassword } from '../../Validation.js';

const Hlogin = () => {
  const baseurl = process.env.REACT_APP_BASEURL;
  const navigate = useNavigate();
  // set Initial values for login
  const [formValue, setFormValue] = useState({
    email: '',
    password: ''
  });

  // handle errors
  const [error, setError] = useState({});

  const handleChange = (event) => {
    setFormValue({ ...formValue, [event.target.name]: event.target.value });
  };

  // code for the login with the function handleSubmit
  const handleSubmit = async (e) => {
    e.preventDefault();
    const newErrors = {};

    if (!validateEmail(formValue.email, true, true)) {
      newErrors.email = 'Please enter a valid email';
    }

    if (!validatePassword(formValue.password, true, true)) {
      newErrors.password =
        'Password must be atleast 8 character with one special character';
    }
    setError(newErrors);

    if (Object.keys(newErrors).length === 0) {
   
      try {
        // Call the login API
        const response = await axios.post(
          `${baseurl}/user/login`,
          formValue
        );
        //Assuming the login API returns user details upon successful login
      
        const token = response.data.token;
        const id= response.data.user.id;
      console.log(token)
        
        if (token && id) {
          localStorage.setItem('token', token);
          localStorage.setItem('id', id);
          // Proceed with navigation or other actions
          // console.log(token);
          // console.log(id)
        } else {
          console.error('Token is undefined in the response');
          alert("Login Failed")
        }
        navigate('/hospital/formManager');

      } catch (error) {
        // Handle login failure, display error message or take appropriate action
        console.error('Login failed:', error);
        alert(error.message);
      }
    }
  };

  return (
    <div className="body1">
      <div className="login-root">
        <div
          className="box-root flex-flex flex-direction--column"
          style={{ minHeight: '100vh', flexGrow: 1 }}
        >
          <div className="loginbackground box-background--white padding-top--64">
            <div className="loginbackground-gridContainer">
              <div
                className="box-root flex-flex"
                style={{ gridArea: 'top / start / 8 / end' }}
              >
                <div
                  className="box-root"
                  style={{
                    backgroundImage:
                      'linear-gradient(white 0%, rgb(247, 250, 252) 33%)',
                    flexGrow: 1
                  }}
                ></div>
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: '4 / 2 / auto / 5' }}
              >
                <div
                  className="box-root box-divider--light-all-2 animationLeftRight tans3s"
                  style={{ flexGrow: 1 }}
                ></div>
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: '6 / start / auto / 2' }}
              >
                <div
                  className="box-root box-background--blue800"
                  style={{ flexGrow: 1 }}
                ></div>
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: '7 / start / auto / 4' }}
              >
                <div
                  className="box-root box-background--blue animationLeftRight"
                  style={{ flexGrow: 1 }}
                ></div>
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: '8 / 4 / auto / 6' }}
              >
                <div
                  className="box-root box-background--gray100 animationLeftRight tans3s"
                  style={{ flexGrow: 1 }}
                ></div>
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: '2 / 15 / auto / end' }}
              >
                <div
                  className="box-root box-background--cyan200 animationRightLeft tans4s"
                  style={{ flexGrow: 1 }}
                ></div>
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: '3 / 14 / auto / end' }}
              >
                <div
                  className="box-root box-background--blue animationRightLeft"
                  style={{ flexGrow: 1 }}
                ></div>
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: '4 / 17 / auto / 20' }}
              >
                <div
                  className="box-root box-background--gray100 animationRightLeft tans4s"
                  style={{ flexGrow: 1 }}
                ></div>
              </div>
              <div
                className="box-root flex-flex"
                style={{ gridArea: '5 / 14 / auto / 17' }}
              >
                <div
                  className="box-root box-divider--light-all-2 animationRightLeft tans3s"
                  style={{ flexGrow: 1 }}
                ></div>
              </div>
            </div>
          </div>
          <div
            className="box-root padding-top--24 flex-flex flex-direction--column"
            style={{ flexGrow: 1, zIndex: 9 }}
          >
            <div className="box-root padding-top--48 padding-bottom--24 flex-flex flex-justifyContent--center">
              <h1 className="drheading">
                <a rel="dofollow">Hospital Login</a>
              </h1>
            </div>
            <div className="formbg-outer">
              <div className="formbg login">
                <div className="formbg-inner">
                  <span className="padding-bottom--15">Please sign in</span>
                  <form id="stripe-login" onSubmit={handleSubmit}>
                    <div className="field padding-bottom--24">
                      <label htmlFor="UHID">Email</label>
                      <input
                        type="text"
                        name="email"
                        placeholder="email/id"
                        required
                        onChange={handleChange}
                        value={formValue.email}
                      />
                      {error.email && (
                        <span className="text text-danger small">{error.email}</span>
                      )}
                    </div>

                    <div className="field padding-bottom--24">
                      <label htmlFor="">Password</label>
                      <input
                        type="password"
                        name="password"
                        placeholder="password"
                        required
                        onChange={handleChange}
                        value={formValue.password}
                      />
                      {error.password && (
                        <span className="text text-danger small">{error.password}</span>
                      )}
                    </div>
                    <div className="col-md-6">
                      <button
                        type="button"
                        className="btn btn-primary submit-button"
                        onClick={handleSubmit}
                      >
                        Submit
                      </button>
                    </div>
                  </form>
                </div>
              </div>
              <div className="footer-link padding-top--24">
                <span>
                  Don't have an account? <Link to="/register">Sign up</Link>
                </span>
                <div className="listing padding-top--24 padding-bottom--24 flex-flex center-center">
                  <span>
                    <Link to="https://tecraki.io">© Tecraki Technology Solutions</Link>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hlogin;
