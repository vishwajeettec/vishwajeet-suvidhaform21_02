
import React, { useState } from 'react';
import "./Auth.css"
import { Link } from 'react-router-dom';
import axios from 'axios';
import { useNavigate } from "react-router-dom";




const Hregister = () => {
    // const baseurl = process.env.REACT_APP_BASEURL;
   
const navigate = useNavigate()
const baseurl=process.env.REACT_APP_BASEURL;
    const [hospitalreg, setHospitalreg] = useState({
     
      name: "",
      logo: "",
      email:"",
      phone_primary: "",
      phone_secondary:"",
      address: "",
    //   state_id:"",
    //   city_id: "",
      pincode: "",
      website: "",
      password:"",
      conpassword:"",
    });
  

const handleRegister = (e) => {
    // console.log(req.body)
    e.preventDefault()

       // Check if password and confirm password match
       if (hospitalreg.password !== hospitalreg.conpassword) {
        alert("Password and Confirm Password do not match");
        return;
    }

    const formData = new FormData();
    formData.append('name', hospitalreg.name);
    formData.append('logo', hospitalreg.logo);
    formData.append('email', hospitalreg.email);
    formData.append('phone_primary', hospitalreg.phone_primary);
    formData.append('phone_secondary', hospitalreg.phone_secondary);
    formData.append('address', hospitalreg.address);
    // formData.append('state_id', hospitalreg.state_id);
    // formData.append('city_id', hospitalreg.city_id);
    formData.append('pincode', hospitalreg.pincode);
    formData.append('website', hospitalreg.website);
    formData.append('password', hospitalreg.password);
    formData.append('conpassword', hospitalreg.conpassword);
   

    axios.post(`${baseurl}/hospital/addHospital`, formData)
    // axios.post('http://localhost:5000/api/hospital/addHospital', formData)
    .then(result => 
      {
        if(result.data.Status) {
            navigate('/login')
        } else {
            alert(result.data.msg)
        }
    })
    .catch(error => {
        if (error.response && error.response.data && error.response.data.error) {
            // Handle specific backend validation errors
            alert(error.response.data.error);
        } else {
            // Handle other types of errors
            console.error("Error during registration:", error.message);
        }
    });
}

 
    return (
        <div className="body1">
            <div className="login-root">
                <div className="box-root flex-flex flex-direction--column" style={{ minHeight: '100vh', flexGrow: 1 }}>
                    <div className="loginbackground box-background--white padding-top--64">
                        <div className="loginbackground-gridContainer">
                            <div className="box-root flex-flex" style={{ gridArea: 'top / start / 8 / end' }}>
                                <div className="box-root" style={{
                                    backgroundImage: 'linear-gradient(white 0%, rgb(247, 250, 252) 33%)',
                                    flexGrow: 1
                                }}></div>
                            </div>
                            <div className="box-root flex-flex" style={{ gridArea: '4 / 2 / auto / 5' }}>
                                <div className="box-root box-divider--light-all-2 animationLeftRight tans3s" style={{ flexGrow: 1 }}></div>
                            </div>
                            <div className="box-root flex-flex" style={{ gridArea: '6 / start / auto / 2' }}>
                                <div className="box-root box-background--blue800" style={{ flexGrow: 1 }}></div>
                            </div>
                            <div className="box-root flex-flex" style={{ gridArea: '7 / start / auto / 4' }}>
                                <div className="box-root box-background--blue animationLeftRight" style={{ flexGrow: 1 }}></div>
                            </div>
                            <div className="box-root flex-flex" style={{ gridArea: '8 / 4 / auto / 6' }}>
                                <div className="box-root box-background--gray100 animationLeftRight tans3s" style={{ flexGrow: 1 }}></div>
                            </div>
                            <div className="box-root flex-flex" style={{ gridArea: '2 / 15 / auto / end' }}>
                                <div className="box-root box-background--cyan200 animationRightLeft tans4s" style={{ flexGrow: 1 }}></div>
                            </div>
                            <div className="box-root flex-flex" style={{ gridArea: '3 / 14 / auto / end' }}>
                                <div className="box-root box-background--blue animationRightLeft" style={{ flexGrow: 1 }}></div>
                            </div>
                            <div className="box-root flex-flex" style={{ gridArea: '4 / 17 / auto / 20' }}>
                                <div className="box-root box-background--gray100 animationRightLeft tans4s" style={{ flexGrow: 1 }}></div>
                            </div>
                            <div className="box-root flex-flex" style={{ gridArea: '5 / 14 / auto / 17' }}>
                                <div className="box-root box-divider--light-all-2 animationRightLeft tans3s" style={{ flexGrow: 1 }}></div>
                            </div>
                        </div>
                    </div>
                    <div className="box-root padding-top--24 flex-flex flex-direction--column" style={{ flexGrow: 1, zIndex: 9 }}>
                        <div className="box-root padding-top--48 padding-bottom--24 flex-flex flex-justifyContent--center">
                            <h1 className="heading-page">
                                <Link to="#" rel="dofollow">Hospital Registration</Link>
                            </h1>
                        </div>
                        <div className="formbg-outer">
                            <div className="formbg">
                                <div className="formbg-inner padding-horizontal--48 col-12">
                                    <form enctype="multipart/form-data" method="post" className="row g-3" onSubmit={handleRegister}>
                                        <div className="col-md-6 field padding-bottom--24">
                                            <label className="form-label">Hospital Name</label>
                                            <input type="text" className="form-control" placeholder="Hospital name" id="hospitalname" name="name" aria-label="Hospital name"  onChange={(e) =>setHospitalreg({ ...hospitalreg, name: e.target.value }) }/>
                                        

                                            {/* {<span className='small'>{errors.name}</span> && <span className='text text-danger small'>{errors.name}</span>} */}
                                           
                                        </div>
                                        <div className="col-md-4 field padding-bottom--24">
                                            <label htmlFor="hospitallogo" className="form-label">Hospital Logo</label>
                                            <input type="file" id="logo" className="form-control" name='logo'  onChange={(e) =>setHospitalreg({ ...hospitalreg, logo: e.target.files[0] }) } />
                                        </div>
                                        {/* <div className="col-md-2 field padding-bottom--24">
                                            {selectedImage && <img src={selectedImage} width="100px" height="100px" alt="Hospital Logo" />}
                                        </div> */}

                                        <div className="col-md-6 field padding-bottom--24">
                                            <label className="form-label">Hospital Email</label>
                                            <input type="email" className="form-control" id="" placeholder="example@email.com"
                                                name="email" onChange={(e) =>setHospitalreg({ ...hospitalreg, email: e.target.value }) } />
                                            {/* {<span className='small'>{errors.email}</span> && <span className='text text-danger small'>{errors.email}</span>} */}
                                        </div>

                                        <div className="col-md-6 field padding-bottom--24">
                                            <label className="form-label">Hospital Primary Phone Number</label>
                                            <input type="tel" className="form-control" id="" name="phone_primary"
                                                placeholder="phone_primary" onChange={(e) =>setHospitalreg({ ...hospitalreg, phone_primary: e.target.value }) }  />
                                            {/* {<span className='small'>{errors.phone1}</span> && <span className='text text-danger small'>{errors.phone1}</span>} */}
                                        </div>

                                        <div className="col-md-6 field padding-bottom--24">
                                            <label className="form-label">Hospital Secondray Phone Number</label>
                                            <input type="tel" className="form-control" id="" name="phone_secondary"
                                                placeholder="phone_secondary" onChange={(e) =>setHospitalreg({ ...hospitalreg, phone_secondary: e.target.value }) }  />
                                            {/* {<span className='small'>{errors.phone2}</span> && <span className='text text-danger small'>{errors.phone2}</span>} */}
                                        </div>

                                        <div className="col-md-6 field padding-bottom--24">
                                            <label className="form-label">Hospital Website</label>
                                            <input className="form-control" id="" placeholder="www.hospi123.com"
                                                name="website" onChange={(e) =>setHospitalreg({ ...hospitalreg, website: e.target.value }) } />
                                            {/* {<span className='small'>{errors.website}</span> && <span className='text text-danger small'>{errors.website}</span>} */}
                                        </div>

                                        <div className="col-md-12 field padding-bottom--24">
                                            <label htmlFor="inputAddress" className="form-label">Hospital Address</label>
                                            <input type="text" className="form-control" id="inputAddress" placeholder="1234 Main St"
                                                name="address" onChange={(e) =>setHospitalreg({ ...hospitalreg, address: e.target.value }) } />
                                            {/* {<span className='small'>{errors.address}</span> && <span className='text text-danger small'>{errors.address}</span>} */}
                                        </div>

                                        <div className="col-md-4 field padding-bottom--24">
                                            <label htmlFor="inputState" className="form-label">State</label>
                                            <select name="state_id"  className='form-control' onChange={(e) =>setHospitalreg({ ...hospitalreg, state_id: e.target.value }) }>
                                                <option value="">Select State</option>
                                                <option value="12">State 1</option>
                                                <option value="23">State 2</option>
                                            </select>
                                            {/* <select id="stateSelect" name="state_id" className="form-select padding-bottom--24" onChange={handleChange}>
                                                <option value="1">State1</option>
                                                {states.map(state => (
                                                    <option key={state.id} value={state.id}>{state.name}</option>
                                                ))}
                                            </select> */}

                                            {/* <input type="number" name="state_id" className='form-fontrol' /> */}
                                            {/* {<span className='small'>{errors.state}</span> && <span className='text text-danger small'>{errors.state}</span>} */}
                                        </div>

                                        <div className="col-md-5 field padding-bottom--24">
                                            <label htmlFor="inputCity" className="form-label">City</label>

                                            <select name="city_id"  className='form-control'  onChange={(e) =>setHospitalreg({ ...hospitalreg, city_id: e.target.value }) }>
                                                <option value="">Select City</option>
                                                <option value="12">City 1</option>
                                                <option value="23">City 2</option>
                                            </select>
                                            {/* <select id="citySelect" name="city_id" className="form-select padding-bottom--24" onChange={handleChange}>
                                                <option value="1">city1</option>
                                                {cities.map(city => (
                                                    <option key={city.id} value={city.id}>{city.name}</option>
                                                ))}
                                            </select> */}
                                            {/* <input type="number" name='city_id' className='form-fontrol' /> */}
                                            {/* {<span className='small'>{errors.city}</span> && <span className='text text-danger small'>{errors.city}</span>} */}

                                        </div>

                                        <div className="col-md-3 field padding-bottom--24">
                                            <label htmlFor="inputZip" className="form-label">Pincode</label>
                                            <input type="text" className="form-control" id="inputZip" name="pincode" onChange={(e) =>setHospitalreg({ ...hospitalreg, pincode: e.target.value }) } />
                                            {/* {<span className='small'>{errors.pincode}</span> && <span className='text text-danger small'>{errors.pincode}</span>} */}
                                        </div>
                                        <div className="col-md-6 field padding-bottom--24">
                                            <label htmlFor="inputPassword4" className="form-label">Password</label>
                                            <input type="password" className="form-control" id="inputPassword4" name="password"
                                                placeholder="password" onChange={(e) =>setHospitalreg({ ...hospitalreg, password: e.target.value }) } />
                                            {/* {<span className='small'>{errors.password}</span> && <span className='text text-danger small'>{errors.password}</span>} */}
                                        </div>
                                        <div className="col-md-6 field padding-bottom--24">
                                            <label htmlFor="confirmPassword" className="form-label">
                                                Confirm Password</label>
                                            <input type="password" className="form-control" id="inputPassword4"
                                                name="password_confirmation" placeholder="confirmPassword"  onChange={(e) =>setHospitalreg({ ...hospitalreg, conpassword: e.target.value }) }  />
                                            {/* {<span className='small'>{errors.cnfPassword}</span> && <span className='text text-danger small'>{errors.cnfPassword}</span>} */}
                                        </div>

                                        <div className="col-6 field padding-bottom--24">
                                            <button type="submit" className="signup-button btn btn-primary">
                                                Register
                                            </button>
                                        </div>
                                        <p>
                                            Already have a account
                                            <Link to="/login"> Login Here </Link>
                                        </p>
                                    </form>
                                </div>
                            </div>
                            <div className="footer-link padding-top--24">
                                <div className="listing padding-top--24 padding-bottom--24 flex-flex center-center">
                                    <span><Link target='_blank' to="https://tecraki.io">© Tecraki Technology Solutions </Link></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

                                        }

export default Hregister;
