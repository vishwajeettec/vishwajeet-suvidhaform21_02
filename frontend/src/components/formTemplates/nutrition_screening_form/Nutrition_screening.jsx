import React from 'react';
import "./Nutritionscreening.css";

const Nurition_screening = () => {
    return (
        <div>
             <div class="header">
        <div class="logo">
            <img class="logo-img" id="hospitalLogo" src="../assets/img/logo-removebg.png" alt="" />
        </div>
        <div class="heading">
            <h1 id="hospitalName">C.B. MEMORIAL NURSING HOME</h1>
            <p id="hospitalAddress">
                K-67, Palhera Chouraha, Pallawpuram Phase-2, Meerut
            </p>
            <p>
                Phone - <span id="phone1">0121-43335598</span>,
                <span id="phone2"> 8979186366</span>
            </p>
        </div>
    </div>
    <hr class="seprator" />
    <div class="form-container">
        <div class="form-heading">
            <h3>NUTRITION ASSESSMENT SCREENING FORM</h3>
        </div>
        <div class="form-details">
            <div class="line1">
                <div class="name">
                    <p> Patient Name :.......................................................</p>
                </div>
                <div class="doctor">
                    <p>Doctor Name :..............................................</p>
                </div>
            </div>
            <div class="line1">
                <div class="serial">
                    <p> Age :.........................................</p>
                </div>
                <div class="serial">
                    <p>Sex :..........................</p>
                </div>
                <div class="serial">
                    <p>Ward No. :.........................</p>
                </div>
                <div class="serial">
                    <p>IDP No. :.........................</p>
                </div>
            </div> 
        <div class="line1">
            <div class="diagnosis">
                <p>
                    Diagnosis :.................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="local">
                <p>
                  Height :............
                </p>
            </div>
            <div class="local">
                <p>
                  CM :.................
                </p>
            </div>
            <div class="local">
                <p>
                  Weight :..................
                </p>
            </div>
            <div class="local">
                <p>
                  KG :................
                </p>
            </div>
            <div class="local">
                <p>
                  BMI :...........
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="sonof">
                <p>
                    Religious Belief : No Onion / No Garlic / Veg.Non Veg / Vegetarian
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="sonof">
                <p>
                    Food Aliegies :..............................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="sonof">
                <p>
                    Diet Prescription Instruction by Treating Consultant : Yes/No (Specific if any..........)
                </p>    
            </div>
        </div>
        <div class="line1">
            <div class="sonof">
                <p>
                    R/T Feed / Liquid / Clear Fluids / Semi solids / Soft / Normal / Full Diet / NPO
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="special">
                <p>
                    Special (Paediatrics) :-
                    </p>
            </div>
        </div>
        <div class="line1">
            <div class="list4">
                <ol type="a">
                    <li>High Protien/High Fat</li>
                    <li>Fluid Restriction</li>
                    <li>Feeding</li>
                    <li>Supplement(if any).............</li>
                </ol>
            </div>
        </div>
        <div class="line1">
            <div class="special">
                <p>
                    Centena :- (pick all that apply)
                </p>    
            </div>
        </div>
        <div class="line1">
                <table class="width">
                    <tr>
                      <th>Adult</th>
                      <th>Pediatrics</th> 
                    </tr>
                    <tr>
                      <td>Weight Loos  5kg./Month</td>
                      <td>Malnutrition</td>
                    </tr>
                    <tr>
                      <td>Difficulty Chewing and Swallowing</td>
                      <td>UNEXPALINED wEIGHT lOSS</td>
                    </tr>
                    <tr>
                      <td>Diabetic Mellitus</td>
                      <td>Inborn Error Of Metabolism</td>
                    </tr>
                    <tr>
                        <td>Renal,Failure</td>
                        <td>Renal Failure</td>
                      </tr>
                      <tr>
                        <td>Hepatic Dysfunction</td>
                        <td>Hepatic Dysfunction</td>
                      </tr>
                      <tr>
                        <td>Cardiac Disorder</td>
                        <td>Cardiac Disorder</td>
                      </tr>
                      <tr>
                        <td>Malnutrition / Cachexia</td>
                        <td>Diarrea and / Or Vomiting</td>
                      </tr>
                      <tr>
                        <td>Diarrehea  and/ or Vomiting</td>
                        <td>New Onset Diabetes</td>
                      </tr>
                      <tr>
                        <td>Multi birth And/Or Lactating</td>
                        <td>Obesity</td>
                      </tr>
                      <tr>
                        <td>Peptic Ulcer Disease</td>
                        <td>Diagnosed Cancer And Undergoing Treatment</td>
                      </tr>
                      <tr>
                        <td>Diagnosed Cancer And Undergoing Treatment</td>
                        <td>In Weaning Period</td>
                      </tr>
                      <tr>
                        <td>Enteral Or Parenteral Nutrition Support</td>
                        <td>Admitted in the ICU/Nursery</td>
                      </tr>
                      <tr>
                        <td>Weight Gain</td>
                        <td>Enteral Or Parenteral Nutrition Support</td>
                      </tr>
                      <tr>
                        <td>Hypertension</td>
                        <td>Cardiac Failure</td>
                      </tr>
                      <tr>
                        <td>koch Lung/Coad/Asthma</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Hypoglycaemia</td>
                        <td></td>
                      </tr>
                  </table>    
            </div>
    </div>
</div>
        </div>

 );
};

    export default Nurition_screening;