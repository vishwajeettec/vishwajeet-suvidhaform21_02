import React from 'react'
import { useState, useEffect } from "react";
import axios from 'axios';
import "./HivTest.css";
const Hiv_test = () => {
    const baseurl = process.env.REACT_APP_BASEURL;

    const [hospitaldetails, setHospitaldetails] = useState({
      hositalName: "HOSPITAL",
      phone1: "9648316865",
      phone2: "9648316865",
      email: "<EMAIL>",
      address: "zyz addrss",
      city: "Gaziabad",
      state: "Uttar Pradesh",
    });
  
    useEffect(() => {
      // try {
      //   const storedHospitaldetails = JSON.parse(
      //     localStorage.getItem("hospitaldetails")
      //   );
      //   if (storedHospitaldetails) {
      //     setHospitaldetails(storedHospitaldetails);
      //   } else {
      //     axios.get(`${baseurl}`).then((response) => {
      //       setHospitaldetails(response.data);
      //     });
      //   }
      // } catch (error) {
      //   console.error(error);
      // }
    });
  
    const [attendantData, setAttendantData] = useState({
      attendant_name: "",
      address: "",
      occupation: "",
      telephone: "",
      relation_with_patient: "",
      patient_id: "",
      signature: "",
    });
  
    const [patientDetails, setPatientDetails] = useState({
      name: "",
      address: "",
      phone: "",
      // Add other fields here as needed
    });
  
    useEffect(() => {
      const storedPatientDetails = JSON.parse(
        localStorage.getItem("patientDetails")
      );
      if (storedPatientDetails) {
        setPatientDetails(storedPatientDetails);
      }
    }, []);
  
    //  for Attedants ============================================================================================
    // codde for attendent data submission
    const onChangeHandlers = (e) => {
      const { name, value } = e.target;
      setAttendantData((prevState) => ({
        ...prevState,
        [name]: value,
      }));
    };
  
    const handleSubmit = async (event) => {
      event.preventDefault();
      try {
        // Assuming you make an API call to submit the attendant data
        // const response = await axios.post(
        //   `http://localhost:5000/api/attendant/`,
        //   attendantData
        // );
        // After successful submission, update patientDetails with attendantData
        // For demonstration, let's assume the submission was successful
  
        // Update patientDetails with attendantData
        setPatientDetails(attendantData);
        alert("Attendant data added successfully");
  
        console.log("Attendant data submitted successfully:", attendantData);
      } catch (error) {
        console.log("Error submitting attendant data:", error);
      }
  
      if (attendantData) {
        document.getElementById("attendant_name").innerHTML=attendantData.attendant_name;
        document.getElementById("attendant_address").innerHTML=attendantData.address;
        document.getElementById("attendant_occupation").innerHTML=attendantData.occupation;
        document.getElementById("attendant_phone").innerHTML=attendantData.telephone;
        document.getElementById("attendant_relation").innerHTML=attendantData.relation_with_patient;
        document.getElementById("attendant_sign").innerHTML=attendantData.signature;
      } else {
        // Set inner HTML to default value or empty string
        document.getElementById("attendant_name").innerHTML = " ";
        document.getElementById("attendant_address").innerHTML = " ";
        document.getElementById("attendant_occupation").innerHTML = " ";
        document.getElementById("attendant_phone").innerHTML = " ";
        document.getElementById("attendant_relation").innerHTML = " ";
        document.getElementById("attendant_sign").innerHTML = " ";
        console.log("No attendant data submitted");
      }
    };
  
    // ============== print form =============================
  
    const handlePrint = () => {
      window.print();
    };
    //  ==================== Doctor list =================
    const [doctors, setDoctors] = useState([]);
    useEffect(() => {
      // try {
      //   axios.get('http://localhost:5000/api/doctors').
      //   then((response) =>
      //   setDoctors(response)
      //   );
      // } catch (error) {
      //   console.log(error);
      // }
    });
  
  return (
    <div>
  <div className="header">
        <div className="logo">
          <img
            className="logo-img"
            src=""
            alt="Hospital Logo"
            style={{ width: "60px", height: "50px" }}
          />
        </div>
        <div className="heading">
          <b>
            <h1 id="hospitalName">{hospitaldetails.hositalName}</h1>
          </b>
          <p id="hospitalAddress">
            <b>
              {hospitaldetails.address +
                " , " +
                hospitaldetails.city +
                " , " +
                hospitaldetails.state}
            </b>
          </p>
          <p>
            <b>
              Phone - {hospitaldetails.phone1 + " ," + hospitaldetails.phone2}
            </b>
          </p>
        </div>

        <div className="attendant-detail">
          <button
            type="button"
            className="btn btn-primary"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
          >
            Fill Attendant Details
          </button>
          <div className="doctor">
            <div className="dropdown">
              <button
                className="btn btn-primary dropdown-toggle"
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Select Doctor
              </button>
              <ul className="dropdown-menu">
                {doctors.map((doctor) => (
                  <li>doctor.name</li>
                ))}
              </ul>
            </div>
          </div>
          <div
            className="modal fade"
            id="exampleModal"
            tabIndex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h1 className="modal-title fs-5" id="staticBackdropLabel">
                    Please Fill the deatils of Attendant
                  </h1>
                  <button
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div className="modal-body">
                  <div className="modal-header"></div>
                  <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                      <label htmlFor="name" className="form-label">
                        Name / नाम
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="name"
                        placeholder="Enter name"
                        name="attendant_name"
                        value={attendantData.attendant_name}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="address" className="form-label">
                        Address / पता
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="address"
                        placeholder="Enter address"
                        name="address"
                        value={attendantData.address}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="occupation" className="form-label">
                        Occupation / व्यवसाय
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="occupation"
                        placeholder="Enter occupation"
                        name="occupation"
                        value={attendantData.occupation}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="telephone" className="form-label">
                        Telephone / दूरभाष
                      </label>
                      <input
                        type="tel"
                        className="form-control"
                        id="telephone"
                        placeholder="Enter telephone"
                        name="telephone"
                        value={attendantData.telephone}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="relation" className="form-label">
                        Relation with Patients (if any) / संबंध
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="relation"
                        placeholder="Enter relation"
                        name="relation_with_patient"
                        value={attendantData.relation_with_patient}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="relation" className="form-label">
                        Digital Signature
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="sign"
                        placeholder="Sign Please"
                        name="signature"
                        value={attendantData.signature}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <button type="submit" className="btn btn-primary">
                      Submit
                    </button>
                  </form>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <hr class="seprator" />
    <div class="form-container">
        <div class="form-heading">
            <h3>CONSENT FORM FOR HIV TESTING</h3>
        </div>
        <div class="form-details">
            <div class="line1">
                <div class="chief">
                    <p>
                      I understand that the test result will part of my confidential medical record.
                      I have been infromed about AIDS(HIV) antibody test see the attached infromation.
                      I had certain question to ask whic have been satisfactorily answered.                  
                    </p>
                </div>
            </div>
            <div class="line1">
                <div class="name">
                    <p> Patient Name :................................................</p>
                </div>
                <div class="sex">
                    <p>Age/Sex:.....................................................</p>
                </div>
            </div>
            <div class="line1">
                <div class="marital">
                    <p>Marital Status : ......................................................</p>
                </div>
                <div class="referred">
                    <p>Referred by : .........................................................</p>
                </div>
            </div>
            <div class="line1">
                <div class="hivstatus">
                    <p>I , hereby , give my consent for the test to be conducted 
                        on me in order to ascertain my HIV 
                        serostatus
                    </p>
                </div>
            </div>
            <div class="line1">
                <div class="signature">
                    <p>Patient Signature </p>
                </div>
                <div class="parent_signature">
                    <p>(In case of minor-Parents Signature)</p>
                </div>
           </div>
           <div class="line1">
            <div class="nextsignature">
                <p>
                    (In case of unconscious patient Signature of next of kin)               
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="dets">
                <p>
                    Specimen :-............................................................
                </p>
            </div>
            <div class="dets">
                <p>Clinical Details :- ...........................................</p>
            </div>
        </div>
        <div class="line1">
            <div class="chief">
                <p>
                  This is to certify that the consent form has been signed in my presence and patient
                  has been given pre testing counselling and post test counsellingis ensured.
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="ambulatory">
                <p>
                   Doctors Signature 
                </p>
            </div>
        </div>
    </div>
</div>
    </div>
  );
};

export default Hiv_test;
