import React from "react";
import "./Hivpretesting.css";

const Hiv_testing = () => {
    return (
        <div>
            <div class="header">
      <div class="logo">
        <img
          class="logo-img"
          id="hospitalLogo"
          src="../assets/img/logo-removebg.png"
          alt=""
        />
      </div>
      <div class="heading">
        <h1 id="hospitalName">C.B. MEMORIAL NURSING HOME</h1>
        <p id="hospitalAddress">
          K-67, Palhera Chouraha, Pallawpuram Phase-2, Meerut
        </p>
        <p>
          Phone - <span id="phone1">0121-43335598</span>,
          <span id="phone2"> 8979186366</span>
        </p>
      </div>
    </div>
    <hr class="seprator" />
    <div class="form-container">
      <div class="form-heading">
        <h3>HIV Pre - Testing Information and Consent</h3>
      </div>
      <div class="pre-test-info">
        <h5><b>Pre - Testing Information:</b></h5>
        <ol>
          <li>
            HIV-Human Immunodeficiency Virus. Also known as HTLV (human t-cell
            lymphotropic virus type III) or LAV (Lymphadenopathy Associated
            Virus.) This is the "AIDS" Virus.
          </li>
          <li>
            HIV Antibody Test I & II this is the initial test for detection of
            exposure to HIV in the blood. Antibodies are substance made by the
            body to fight infection
          </li>
        </ol>
      </div>
      <div class="pre-test-info">
        <h5><b>A Negative Test can mean :</b></h5>
        <ol>
          <li>
            That you have not been exposed to or infected by the AIDS virus
          </li>
          <li>
            That you have been exposed to the AIDS virus, but your body has not
            had enough time to make antibodies. If his this is possibility, your
            doctor may recommend repeating the lest during the next 3 to 6
            months
          </li>
          <li>That the test was falsely negative for technical reasons.</li>
        </ol>
      </div>
      <div class="pre-test-info">
        <h5><b>A Positive Test can mean :</b></h5>
        <ol>
          <li>
            You do not necessarily have AIDS. Your blood should be sent for
            another, more specific and confirmatory test.
          </li>
          <li>
            If another HIV antibody test return positive, then you most likely
            been exposed to and have AIDS virus and you should consider yourself
            capable of passing the virus to other.
          </li>
        </ol>
      </div>
      <div class="pre-test-info">
        <h5><b>This test it recommended for the following high-risk groups:</b></h5>
        <ol>
          <li>
            Homosexuality even one homosexual encounter, since 1976 puts a
            person at risk
          </li>
          <li>Bisexuality</li>
          <li>
            Blood transfusions any transfusion between 1976 and 1985 may have
            caused infectio
          </li>
          <li>
            Multiple sexual partners Multiple heterosexual or homosexual
            partners increase th infection.
          </li>
          <li>
            Prostitution Persons born in a country with a high incidence of
            heterosexual tra persons immigrating from Haiti, Central Africa etc
            since 1977 are at increased risk. It is in mention that PREVENTION
            is the best policy in dealing with AIDS (or a positive HIV test) and
            transmission it is important to avoid the following.
            <ol>
              <li>Sexual contact with persons suspected of AIDS virus.</li>
              <li>Sexual contact with multiple partners.</li>
              <li>
                Intravenous drug abuse or sexual contact with people who use
                intravenous drugs.
              </li>
              <li>
                Oral-genital contact or open-mouthed kissing with high-risk
                contacts.
              </li>
            </ol>
          </li>
        </ol>
      </div>
      <div class="para">
        <p>
          It is recommended that you and your partner use condoms as protection
          against the transmission of the AIDS virus. However, it should be
          noted that while the use of condoms is considered to be a "Safe Sexual
          Practice", it should not be considered 100% effective, and, therefore,
          it is important for you to choose partners carefully and to avoid high risk
          behaviours as noted in the guideline Please feel free to consult your
          primary care physician with any questions you may have about this  Information.
        </p>
      </div>
      <p><b>Test Result will be confidential</b></p>
    </div>
     
        </div>
    );
};


export default Hiv_testing;