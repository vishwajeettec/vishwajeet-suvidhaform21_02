import React from "react";
import "./Precheckup.css";

const Pre_checkup = () => {
    return(
        <div>
 <div class="header">
        <div class="logo">
            <img class="logo-img" id="hospitalLogo" src="../assets/img/logo-removebg.png" alt="" />
        </div>
        <div class="heading">
            <h1 id="hospitalName">C.B. MEMORIAL NURSING HOME</h1>
            <p id="hospitalAddress">
                K-67, Palhera Chouraha, Pallawpuram Phase-2, Meerut
            </p>
            <p>
                Phone - <span id="phone1">0121-43335598</span>,
                <span id="phone2"> 8979186366</span>
            </p>
        </div>
    </div>
    <hr class="seprator" />
    <div class="form-container">
        <div class="form-heading">
            <h3>PRE ANESTHETIC CHECKUP</h3>
        </div>
        <div class="form-details">
            <div class="line1">
                <div class="name">
                    <p>Name :................................................</p>
                </div>
                <div class="sex">
                    <p>Age/Sex:.........................</p>
                </div>
                <div class="bedno">
                    <p>Bed No. : ......................</p>
                </div>
            </div>
            <div class="line1">
                <div class="surgeon">
                    <p>Surgeon : ...............................................</p>
                </div>
                <div class="anaesthetist">
                    <p>Anaesthetist : ......................</p>
                </div>
                <div class="date">
                    <p>Date : ..............</p>
                </div>
            </div>
            <div class="line1">
                <div class="PreOperativeDiagnosis">
                    <p>Pre Operative Diagnosis : .........................</p>
                </div>
                <div class="proposed">
                    <p>Proposed Surgery : ..............................</p>
                </div>
            </div>
            <div class="line1">
                <div class="briefhistory">
                    <p>Brief History Of Patient :........................................................</p>
                </div>
           </div>
        <div class="line1">
            <div class="co">
                <p>
                    History ofg illness/Anaesthesia/Operation/Drugs/Blood Transfusion
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="list">
                <ol type="1">
                    <li>Cough</li>
                    <li>Wheezing</li>
                    <li>Hypertension</li>
                    <li>Diabetes</li>
                    <li>Liver Problem/Jaundice</li>
                    <li>Previouse Operation</li>
                    <li>Smoking</li>
                    <li>Headeche/Migraine</li>
                    <li>Pregnancy</li>
                    <li>Bleeding Disorder</li>
                    <li>Frequent Urination</li>
                    <li>Arthritis/Painful/Swollen Joint</li>
                    <li>Abnormality of Nerve Of muscle</li>
                    <li>Weight Lss/Gain</li>
                    <li>Fever/chills/Cold/flu</li>
                    <li>Artificial Joint/Plates ect.</li>
                </ol>
            </div>
            <div class="list">
                <ul class="yesno">
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                </ul>
            </div>
            <div class="list">
                <ol start="17">
                    <li>SOB</li>
                    <li>Palpitation</li>
                    <li>CAC/PTCA/CABG/Chest Pain</li>
                    <li>Renal Disease</li>
                    <li>Allergies drug/Food</li>
                    <li>Previous Admisssion</li>
                    <li>Alcohol</li>
                    <li>Seizures/Fainting/Syncope</li>
                    <li>Recent URI/fever</li>
                    <li>Previous Anaesthesia & Problem</li>
                    <li>Back & Neck Pain</li>
                    <li>Blackouts/Loss of Consciousness</li>
                    <li>Blood Transfusion</li>
                    <li>Indigestion/Acid Reflux/Herbum/Hiatus Hernia</li>
                    <li>Contact Lens/Pacemaker Hearing Aid</li>
                </ol>
            </div>
            <div class="list">
                <ul class="yesno">
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                    <li>YES/NO</li>
                </ul>
            </div>
        </div>
        <div class="line1">
            <div class="co">
                <p>
                  32.  Any other Disease :.........................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="co">
                <p>
                  33.  Please List All the Medication(Prescribed and Or have Take in the last six months) 
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="co">
                <p>
                  34.  Are you Allergic to anything?Please list Family History :
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="co">
                <p>
                  Regular Medicines & Details :..................................................
                </p>
            </div>
        </div>
    </div>
</div>
        </div>

    );
};


export default Pre_checkup;