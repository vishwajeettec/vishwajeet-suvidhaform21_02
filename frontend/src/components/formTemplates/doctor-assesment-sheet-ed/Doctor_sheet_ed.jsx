import React from 'react'
import "./Doctorsheet.css";
const doctor_sheet_ed = () => {
  return (
    <div>
         <div class="header">
        <div class="logo">
            <img class="logo-img" id="hospitalLogo" src="../assets/img/logo-removebg.png" alt="" />
        </div>
        <div class="heading">
            <h1 id="hospitalName">C.B. MEMORIAL NURSING HOME</h1>
            <p id="hospitalAddress">
                K-67, Palhera Chouraha, Pallawpuram Phase-2, Meerut
            </p>
            <p>
                Phone - <span id="phone1">0121-43335598</span>,
                <span id="phone2"> 8979186366</span>
            </p>
        </div>
    </div>
    <hr class="seprator" />
    <div class="form-container">
        <div class="form-heading">
            <h3>DOCTOR ASSESMENT SHEET-ED</h3>
        </div>
        <div class="form-details">
            <div class="line1">
                <div class="name">
                    <p>Name :................</p>
                </div>
                <div class="sex">
                    <p>Age:..................</p>
                </div>
                <div class="age">
                    <p>Sex: .................</p>
                </div>
                <div class="age">
                    <p>IPD: ...............</p>
                </div>
                <div class="age">
                    <p>Date: ...............</p>
                </div>
                <div class="age">
                    <p>Time: ................</p>
                </div>
            </div>
        </div>
        <div class="line1">
            <div class="history">
                <p>
                    History :
                    ............................................................................
                </p>
            </div>
        </div>

        <div class="line1">
            <div class="presentingcomplaint">
                <p>
                    Presenting Complaint :
                    ............................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="generalphysical">
                <p>
                    General Physical Examination :
                    ............................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="painscore">
                <p>
                    Pain Score:Verable Rating Scale: (VRS) :    
                </p>
            </div>
            <div class="painscore">
              <div class="nill">
                <p>0</p>
                <p>Nill</p>
              </div>
              <div class="nill">
                <p>1</p>
                <p>Mild</p>
              </div>
              <div class="nill">
                <p>2</p>
                <p>Moderate</p>
              </div>
              <div class="nill">
                <p>3</p>
                <p>Server</p>
              </div>
            </div>
        </div>
        <div class="line1">
            <div class="systemicexamination">
                <p>
                    Systemic Examination :
                </p>
            </div>
        </div>
        <div class="line1">
        <div class="tableout">
        <table>
            <tr>
              <th>GC</th>
              <th>P/R</th>
              <th>BP</th>
              <th>SPO<sub>2</sub></th>
              <th>R/R</th>
              <th>Pallor</th>
              <th>Cyanosis</th>
              <th>Odema</th>
              <th>GCS</th>
              <th>Pupli</th>
              <th>Plantar</th>
              <th>Temp.</th>
              <th>Heart</th>

            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>Chest</td>
              <td>Abd.</td>
              <td>RBS</td>
              <td>Bedsore</td>
              <td>Smoking</td>
              <td>Alcohol</td>
              <td>Drugs</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
          </table>
        </div>
    </div>
    <div class="line1">
        <div class="Provisionaldiagnosis">
            <p>
                Provisional Diagnosis :
                ............................................................................
            </p>
        </div>
    </div>
    </div>
    </div>
  );
};

export default doctor_sheet_ed;
