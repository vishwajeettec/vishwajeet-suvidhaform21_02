import React from 'react'
import "./CarePlan.css";
const Care_plan = () => {
  return (
    <div>
     <div class="header">
        <div class="logo">
            <img class="logo-img" id="hospitalLogo" src="../assets/img/logo-removebg.png" alt="" />
        </div>
        <div class="heading">
            <h1 id="hospitalName">C.B. MEMORIAL NURSING HOME</h1>
            <p id="hospitalAddress">
                K-67, Palhera Chouraha, Pallawpuram Phase-2, Meerut
            </p>
            <p>
                Phone - <span id="phone1">0121-43335598</span>,
                <span id="phone2"> 8979186366</span>
            </p>
        </div>
        <div class="line2">
            <div class="formboxdetails">
                <div class="line2">
                    <div class="name">
                    <p> Patient Name :.....................</p>
                    </div>
                </div>
                <div class="line2">
                    <div class="agesex">
                        <p>Age :..............................</p>
                    </div>
                    <div class="agesex">
                        <p>Sex :..............................</p>
                    </div>
                </div>
                <div class="line2">
                    <div class="bed">
                        <p>Bed No. :......................</p>
                    </div>
                    <div class="bed">
                        <p>IPD No. :.......................</p>
                    </div>
                </div>
                <div class="line2">
                    <div class="bed">

                     <p> Consultant :...........................................................</p>
                    </div>
                 </div>
                 <div class="line2">
                    <div class="bed">
                     <p> Reg. No. :..............................................................</p>
                    </div>
                 </div>
            </div>
         </div> 
         </div> 
    <hr class="seprator" />
    <div class="form-container">
        <div class="form-heading">
            <h3>CARE PLAN</h3>
        </div>
        <div class="form-details">
            <div class="line1">
                <div class="chief">
                    <p>
                     Status at the Beginning Of The Treatment :................................                  
                    </p>
                </div>
            </div>
            <div class="line1">
                <div class="provisional">
                    <p>Provisional Diagnosis: ......................................................</p>
                </div>
            </div>
            <div class="line1">
                <div class="Preventative">
                    <p>Preventative Care : Explaind / Educational Learlet <br/>
                        (Please Write the Explaind Part in Brief)
                    </p>
                </div>
            </div>
            <div class="line1">
                <div class="signature">
                    <p>Progress Chart 2 :-</p>
                </div>
           </div>
            <div class="nextsignature">
                <p class="border">
                    <table>
                      <tr>
                        <th>Sickness</th>
                        <th>DAY 1</th>
                        <th>DAY 2</th>
                        <th>DAY 3</th>
                        <th>DAY 4</th>
                        <th>DAY 5</th>
                        <th>DAY 6</th>
                        <th>DAY 7</th>
                      </tr>
                      <tr>
                        <td>Severe</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Moderate</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>Mild</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>WNL</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                      </tr>
                    </table>               
                </p>
            </div>
        <div class="line1">
            <div class="dets">
                <p>
                    Rehabilitative Remark:
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="chief">
                <p>
                  Anticipated Complications:
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="ambulatory">
                <p>
                  Goals Of Treatment -
                </p>
            </div>
            <div class="ambulatory">
                <p>
                  Curative  <input type="checkbox"></input>
                </p>
            </div>
            <div class="ambulatory">
                <p>
                  Preventive  <input type="checkbox"></input>
                </p>
            </div>
            <div class="ambulatory">
                <p>
                  Rehabilitative  <input type="checkbox"></input>
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="chief">
                <p>
                  Consultations Name & Signature
                </p>
            </div>
        </div>
        <hr class="seprator" />
        <div class="line1">
            <div class="chief">
                <p>
                 Note : If the Patient Stay is more than 7 days , Please fill the Revised Care Plan.
                </p>
            </div>
        </div>
    </div>
 </div>
   

</div>
  );
};

export default Care_plan;
