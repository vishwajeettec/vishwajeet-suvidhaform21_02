import React from "react";
import "./Nurseassesmentsheet.css";


const Nurse_sheet = () => {
    return (
        <div>
             <div class="header">
        <div class="logo">
            <img class="logo-img" id="hospitalLogo" src="../assets/img/logo-removebg.png" alt="" />
        </div>
        <div class="heading">
            <h1 id="hospitalName">C.B. MEMORIAL NURSING HOME</h1>
            <p id="hospitalAddress">
                K-67, Palhera Chouraha, Pallawpuram Phase-2, Meerut
            </p>
            <p>
                Phone - <span id="phone1">0121-43335598</span>,
                <span id="phone2"> 8979186366</span>
            </p>
        </div>
    </div>
    <hr class="seprator" />
    <div class="form-container">
        <div class="form-heading">
            <h3>NURSES ASSESSMENT SHEET-ED</h3>
        </div>
        <div class="form-details">
            <div class="line1">
                <div class="name">
                    <p> Patient Name :................................................</p>
                </div>
                <div class="sex">
                    <p>Age/Sex:.....................................................</p>
                </div>
            </div>
            <div class="line1">
                <div class="consultant">
                    <p>MRD No. : ......................................................</p>
                </div>
                <div class="consultant">
                    <p>Consultant : .........................................................</p>
                </div>
            </div>
            <div class="line1">
                <div class="datetime">
                    <p>Date & Time Receiving the Patient : .........................</p>
                </div>
            </div>
            <div class="line1">
                <div class="mlc">
                    <p>MLC :- </p>
                </div>
                <div class="yes">
                   YES <input type="checkbox" class="box" value="check"/> 
                </div>
                <div class="no">
                    NO <input type="checkbox" class="box" value="check"/> 
                </div>
                <div class="mlcno">
                    MLC No :-.........................................................
                </div>
           </div>
        <div class="line1">
            <div class="weight">
                <p>
                    Weight :-............................................................ID Band
                </p>
            </div>
            <div class="yes1">
                <input type="checkbox" class="box" value="check"/> YES  
            </div>
            <div class="no1">
               <input type="checkbox" class="box" value="check"/> NO
            </div>
        </div>
        <div class="line1">
            <div class="chief">
                <p>
                  Chief Complaints :.........................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="ambulatory">
                <p>
                Ambulatory Non  <input type="checkbox" class="box" value="check"/> 
                </p>
            </div>
            <div class="ambulatory">
                <p>
                Ambulatory <input type="checkbox" class="box"/> 
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="temp">
                <p>
                 Pulse :-..........................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="temp">
                <p>
                 B.P :-.......................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="temp">
                <p>
                 RR :-.......................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="temp">
                <p>
                 Temperature :-.......................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="temp">
                <p>
                 SPO2 :-.......................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="temp">
                <p>
                 RBS :-.......................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="patienton">
                <p>
                    Patient On :- 
                </p>
            </div>
            <div class="patienton">
                <p>
                Room Air    <input type="checkbox" class="box" value="check"/>
                </p>
            </div>
            <div class="patienton">
                <p>
                Oxygen   <input type="checkbox" class="box" value="check"/>
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="ivline">
                <p>
                    IV Line :-  
                </p>
            </div>
            <div class="ivline">
                <p>
                      <input type="checkbox" class="box" value="check"/>
                </p>
            </div>
            <div class="ivline">
                <p>
                     <input type="checkbox" class="box" value="check"/> 
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="ivline">
                <p>
                    Bed Score :-  
                </p>
            </div>
            <div class="ivline">
                <p>
                    <input type="checkbox" class="box" value="check"/> 
                </p>
            </div>
            <div class="ivline">
                <p>
                     <input type="checkbox" class="box" value="check"/> 
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="invesigation">
                <p>
                 Any Previous  Investigation Report :-.......................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="kth">
                <p>
                KTH/IPD/PST/,013
                </p>
            </div>
        </div>
    </div>
</div>
        </div>

    );
};



export default Nurse_sheet;