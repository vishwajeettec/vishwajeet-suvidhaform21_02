import React from "react";
import "./Nurseadmission.css";


const Nurse_admission = () =>{
    return (
        <div>
            <div class="header">
        <div class="logo">
            <img class="logo-img" id="hospitalLogo" src="../assets/img/logo-removebg.png" alt="" />
        </div>
        <div class="heading">
            <h1 id="hospitalName">C.B. MEMORIAL NURSING HOME</h1>
            <p id="hospitalAddress">
                K-67, Palhera Chouraha, Pallawpuram Phase-2, Meerut
            </p>
            <p>
                Phone - <span id="phone1">0121-43335598</span>,
                <span id="phone2"> 8979186366</span>
            </p>
        </div>
    </div>
    <hr class="seprator" />
    <div class="form-container">
        <div class="form-heading">
            <h3>NURSING ADMISSION ASSESSMENT</h3>
        </div>
        <div class="form-details">
            <div class="line1">
                <div class="name">
                    <p>Name of Patient :................................................</p>
                </div>
                <div class="sex">
                    <p>Age/Sex:.........................</p>
                </div>
                <div class="date">
                    <p>Date: ......................</p>
                </div>
            </div>
            <div class="line1">
                <div class="ipd">
                    <p>IPD No/UHID No. :........................................................</p>
                </div>
                <div class="ward">
                    <p>Ward/Bed No.:..........................................</p>
                </div>
           </div>
        <div class="line1">
            <div class="co">
                <p>
                    1. C/O :
                    ............................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="Vulenerable">
                <p>
                    2. Vulenerable :
                    ............................................................................
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="restrain">
                <p>
                   3. Restrain :
                    ............................................................................
                </p>
            </div>
        </div>

        <div class="line1">
            <div class="admission">
                <p>
                    4. Admission Through :
                    ............................................................................
                </p>
            </div>
            <div class="casualty">
                <p>Casualty <input type="checkbox"/></p>
            </div>
            <div class="elective">
                <p>Elective <input type="checkbox"/></p>
            </div>
        </div>
        <div class="line1">
            <div class="Provisional">
                <p>
                    5. Provisional Diagnosis :
                    ............................................................................
                </p>
            </div>
        </div>
        <hr class="hrline"/>
        <div class="line1">
            <div class="Provisional">
                <p>
                   6. Personal Belongings :
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="valuable">
                <p>
                    Valuables.................................Handed Over To (Signature Of Attendant)
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="dentures">
                <p>
                    Dentures : 
                </p>
            </div>
            <div class="none">
                <p>None <input type="checkbox"/></p>
            </div>
            <div class="upper">
                <p>Upper <input type="checkbox"/></p>
            </div>
            <div class="lower">
                <p>Lower <input type="checkbox"/></p>
            </div>
            <div class="partial">
                <p>Partial <input type="checkbox"/></p>
            </div>
        </div>
        <div class="line1">
            <div class="hearing">
                <p>
                    Hearing :
                </p>
            </div>
            <div class="none1">
                <p>None <input type="checkbox"/></p>
            </div>
            <div class="right">
                <p>Right <input type="checkbox"/></p>
            </div>
            <div class="let">
                <p>Let <input type="checkbox"/></p>
            </div>
        </div>
        <div class="line1">
            <div class="pacemaker">
                <p>
                    Pacemaker :
                </p>
            </div>
            <div class="none2">
                <p>None <input type="checkbox"/></p>
            </div>
            <div class="right1">
                <p>Right <input type="checkbox"/></p>
            </div>
        </div>
        <hr class="hrline"/>
        <div class="line1">
            <div class="patientenviroment7">
                <p>
                   7. Patient Opientation to Environment
                </p>
            </div>
        </div>
        <div class="line1">
        <table class="patientlist">
            <div class="line1">
            <tr>
              <td>a. Bed Control</td>
              <td>b. Visiting time</td>
              <td>c. Smoking Policy</td>
            </tr>
            <tr>
              <td>d. Call Bell</td>
              <td>e. Telephone</td>
              <td>f. Cafeteria</td>
            </tr>
            <tr>
              <td>g. Side Rails</td>
              <td>h. Information Booklet</td>
              <td>i. Light control</td>
            </tr>
            </div>
          </table>
        </div>
        <hr class="hrline"/>
        <div class="line1">
            <div class="patientenviroment7">
                <p>
                   8. Allergies/Adverce Reactions
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="Provisional">
                <p>
                    a. Known of suspected Allergies to 
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="Discription">
                <p>
                    b. Name of Medication & Discription Of Reaction 
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="Medication">
                <p>
                    c. Medication Drugs
                </p>
            </div>
            <div class="yes/no">
                <p>
                   YES/NO/NOT KNOWN
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="Blood">
                <p>
                    d. Blood Transfusion 
                </p>
            </div>
            <div class="yes/no">
                <p>
                   YES/NO/NOT KNOWN
                </p>
            </div>
        </div>
        <div class="line1">
            <div class="Food">
                <p>
                    e. Food  
                </p>
            </div>
        </div>
        
        
    </div>
</div>
</div>
    );
};

export default Nurse_admission;