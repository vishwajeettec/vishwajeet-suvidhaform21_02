import React from 'react'
import "./Surgicaloperation.css";
const surgical_operation = () => {
  return (
    <div>
         <div class="header">
        <div class="logo">
            <img class="logo-img" id="hospitalLogo" src="../assets/img/logo-removebg.png" alt="" />
        </div>
        <div class="heading">
            <h1 id="hospitalName">C.B. MEMORIAL NURSING HOME</h1>
            <p id="hospitalAddress">
                K-67, Palhera Chouraha, Pallawpuram Phase-2, Meerut
            </p>
            <p>
                <span id="phone1"> Phone - 0121-43335598, 8979186366</span>
            </p>
        </div>
    </div>
    <hr class="seprator" />
    <div class="form-container">
        <div class="form-heading">
            <h5>
                CONSENT FORM FOR SURGICAL OPERATION AND / OR DIAGNOSTIC /THERAPEUTIC
                PROCEDURE
            </h5>
        </div>
        <div class="patients-details">
            <div class="cr-no">
                <p>C.R No .................</p>
            </div>
            <div class="uhid">
                <p>UHID NO : .............</p>
            </div>
            <div class="date">
                <p>Date : ................</p>
            </div>
        </div>
        <div class="details2">
            <div class="name">
                <p>Name :.................</p>
            </div>
            <div class="sex">
                <p>Sex : ............</p>
            </div>
            <div class="age">
                <p>Age : .........</p>
            </div>
        </div>

        <div class="son">
            <div class="sonof">
                <p>S/o, D/o, W/o : ...................</p>
            </div>
        </div>
        <div class="heading">
            <h4 class="h4">Authorization For Surgical Operation And / or Diagnostic / Therapeutic Procedure
            </h4>
        </div>
        <div class="para">
            <ol>
                <li> I hereby authorize the above hospital and its staff to perform upon the above patient the following
                    surgical Operation and/or diagnostic/therapeutic procdure

                </li>
                <li>
                    It has been explained to me that, during the course of thr operation / procedure unforeseen
                    condition may be revealed or encountered which necessitate surgical or there emergency procedures in
                    addition to or different for those contemplated at the time to initial diagnosis, I therefore,
                    further authorize above designated staff to perform such additional surgical or other procedures as
                    they deem necessary or desirable.

                </li>
                <li>
                    I further consent to the administration of drugs infusions, blood product transfusions or any other
                    treatment or procedure dimmed necessary.

                </li>
                <li>
                    The nature and purpose of the operation and/ or procedures, the necessity thereof, the possible
                    alternative methods. treatment, prognosis, the risks involved and the possibility of complication in
                    the investigative procedures/ investigation and treatment of my condition/diagnosis have been fully
                    explained to me and I have understood the same

                </li>
                <li>
                    I have been given an opportunity to ask all any questions and I have also been given option to ask
                    for second opinion.


                </li>
                <li>
                    I acknowledge that no guarantee and promise has been made to me consuming the result of any
                    procedure/treatment.


                </li>
                <li>
                    I consent to the photographing or televising of the operation or procedures to be performed,
                    including appropriate portions of my body, for medical, scientific of educational purpose, provided
                    my identity is not revealed by pictures or by descriptive texts accompanying them.

                </li>
                <li>
                    I also give consent to the disposal by hospital authorities of any deceased tissued of parts there
                    of necessary to or removed during the course of operative procedure/treatment.
                </li>

            </ol>
        </div>
        <div class="certify">
            <p>CERTIFY THAT THE STATEMENT MADE IN THE ABOVE CONSENT FORM HAS BEEN READ OVER AND EXPLINED TO ME IN MY MOTHER TONGUE AND I HAVE FULLY UNDERSTOOD THE IMPLICATIONS OF THE ABOVE CONSENT.
            </p>
        </div>
        <div class="sign">
            <div class="witness-name-sign">
                <p>Name & Signature of the witness</p>
                <p>--------------------------</p>
                <p>--------------------------</p>
            </div>
            <div class="patients-sign">
                <p>Signature of the patients /Parent/Gurdian</p>
                <p>Thumb Impression</p>
                <p>Name: ......................</p>
                <p>Relationship With Patients : ...................</p>
            </div>
        </div>
        <div class="cerify1">
            <p> I CONFIRM THAT I HAVE EXPLANIED THAT NATURE AND EFFECTS OF THE OPERATION/

                OCEDUE/THREATMENT TO THE PERSON WHO HAS SIGNED THE ABOVE CONSENT FORM
            </p>
        </div>
        <div class="sign-anaes">
            <p> Signature Of Anaesthetist</p>
             <p> Name : ---------------------</p>
        </div>
    </div>

    </div>
  );
};

export default surgical_operation;
