import React, { useState, useEffect } from "react";
import "./Consent_for_treatment.css";
import axios from "axios";
import { Link } from "react-router-dom";
const Consent_for_treatment = () => {
  const baseurl = process.env.REACT_APP_BASEURL;

  const [patientDetails, setPatientDetails] = useState({
    name: '',
    id: '',
    phone: '',
    age: '',
    address: '',
  });

  const handleSearch = async (event) => {
    event.preventDefault();
    try {
      const searchInput = document.querySelector('input[name="searchInput"]');
      const id = searchInput.value;

      const response = await axios.get(`${baseurl}/patients/${id}`);
      const patientData = response.data;

      console.log('Fetched patient data:', patientData); // Debugging statement

      // Update state with patient details
      setPatientDetails(patientData);

      // Store patient details in local storage
      localStorage.setItem('patientDetails', JSON.stringify(patientData));
      console.log('Patient details stored in local storage:', patientData); // Debugging statement

      // Retrieve patient details from local storage
      const storedPatientDetails = JSON.parse(localStorage.getItem('patientDetails'));
      console.log('Retrieved patient details from local storage:', storedPatientDetails); // Debugging statement
    } catch (error) {
      console.error('Error fetching patient details:', error);
    }

  };

  const [hospitaldetails, setHospitaldetails] = useState({
    hositalName: "HOSPITAL",
    phone1: "9648316865",
    phone2: "9648316865",
    email: "<EMAIL>",
    address: "zyz addrss",
    city: "Gaziabad",
    state: "Uttar Pradesh",
  });

  useEffect(() => {
    // try {
    //   const storedHospitaldetails = JSON.parse(
    //     localStorage.getItem("hospitaldetails")
    //   );
    //   if (storedHospitaldetails) {
    //     setHospitaldetails(storedHospitaldetails);
    //   } else {
    //     axios.get(`${baseurl}`).then((response) => {
    //       setHospitaldetails(response.data);
    //     });
    //   }
    // } catch (error) {
    //   console.error(error);
    // }
  });

  const [attendantData, setAttendantData] = useState({
    attendant_name: "",
    address: "",
    occupation: "",
    telephone: "",
    relation_with_patient: "",
    patient_id: "",
    signature: "",
  });

  // const [patientDetails, setPatientDetails] = useState({
  //   name: "",
  //   address: "",
  //   phone: "",
  //   // Add other fields here as needed
  // });

  useEffect(() => {
    const storedPatientDetails = JSON.parse(
      localStorage.getItem("patientDetails")
    );
    if (storedPatientDetails) {
      setPatientDetails(storedPatientDetails);
    }
  }, []);

  //  for Attedants ============================================================================================
  // codde for attendent data submission
  const onChangeHandlers = (e) => {
    const { name, value } = e.target;
    setAttendantData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      // Assuming you make an API call to submit the attendant data
      // const response = await axios.post(
      //   `http://localhost:5000/api/attendant/`,
      //   attendantData
      // );
      // After successful submission, update patientDetails with attendantData
      // For demonstration, let's assume the submission was successful

      // Update patientDetails with attendantData
      setPatientDetails(attendantData);
      alert("Attendant data added successfully");

      console.log("Attendant data submitted successfully:", attendantData);
    } catch (error) {
      console.log("Error submitting attendant data:", error);
    }

    if (attendantData) {
      document.getElementById("attendant_name").innerHTML = attendantData.attendant_name;
      document.getElementById("attendant_address").innerHTML = attendantData.address;
      document.getElementById("attendant_occupation").innerHTML = attendantData.occupation;
      document.getElementById("attendant_phone").innerHTML = attendantData.telephone;
      document.getElementById("attendant_relation").innerHTML = attendantData.relation_with_patient;
      document.getElementById("attendant_sign").innerHTML = attendantData.signature;
    } else {
      // Set inner HTML to default value or empty string
      document.getElementById("attendant_name").innerHTML = " ";
      document.getElementById("attendant_address").innerHTML = " ";
      document.getElementById("attendant_occupation").innerHTML = " ";
      document.getElementById("attendant_phone").innerHTML = " ";
      document.getElementById("attendant_relation").innerHTML = " ";
      document.getElementById("attendant_sign").innerHTML = " ";
      console.log("No attendant data submitted");
    }
  };

  // ============== print form =============================

  const handlePrint = () => {
    window.print();
  };
  //  ==================== Doctor list =================
  const [doctors, setDoctors] = useState([]);
  useEffect(() => {
    // try {
    //   axios.get('http://localhost:5000/api/doctors').
    //   then((response) =>
    //   setDoctors(response)
    //   );
    // } catch (error) {
    //   console.log(error);
    // }
  });

  return (
    <div className="main-header">
      <div className="header">
        <div className="logo">
          <img
            className="logo-img"
            src=""
            alt="Hospital Logo"
            style={{ width: "60px", height: "50px" }}
          />
        </div>
        <div className="heading">
          <b>
            <h1 id="hospitalName">{hospitaldetails.hositalName}</h1>
          </b>
          <p id="hospitalAddress">
            <b>
              {hospitaldetails.address +
                " , " +
                hospitaldetails.city +
                " , " +
                hospitaldetails.state}
            </b>
          </p>
          <p>
            <b>
              Phone - {hospitaldetails.phone1 + " ," + hospitaldetails.phone2}
            </b>
          </p>
        </div>

        {/* <div className="attendant-detail">
          <button
            type="button"
            className="btn btn-primary"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
          >
            Fill Attendant Details
          </button>
          <div className="doctor">
            <div className="dropdown">
              <button
                className="btn btn-primary dropdown-toggle"
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Select Doctor
              </button>
              <ul className="dropdown-menu">
                {doctors.map((doctor) => (
                  <li>doctor.name</li>
                ))}
              </ul>
            </div>
          </div>
          <div
            className="modal fade"
            id="exampleModal"
            tabIndex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h1 className="modal-title fs-5" id="staticBackdropLabel">
                    Please Fill the deatils of Attendant
                  </h1>
                  <button
                    type="button"
                    className="btn-close"
                    data-bs-dismiss="modal"
                    aria-label="Close"
                  ></button>
                </div>
                <div className="modal-body">
                  <div className="modal-header"></div>
                  <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                      <label htmlFor="name" className="form-label">
                        Name / नाम
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="name"
                        placeholder="Enter name"
                        name="attendant_name"
                        value={attendantData.attendant_name}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="address" className="form-label">
                        Address / पता
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="address"
                        placeholder="Enter address"
                        name="address"
                        value={attendantData.address}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="occupation" className="form-label">
                        Occupation / व्यवसाय
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="occupation"
                        placeholder="Enter occupation"
                        name="occupation"
                        value={attendantData.occupation}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="telephone" className="form-label">
                        Telephone / दूरभाष
                      </label>
                      <input
                        type="tel"
                        className="form-control"
                        id="telephone"
                        placeholder="Enter telephone"
                        name="telephone"
                        value={attendantData.telephone}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="relation" className="form-label">
                        Relation with Patients (if any) / संबंध
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="relation"
                        placeholder="Enter relation"
                        name="relation_with_patient"
                        value={attendantData.relation_with_patient}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="relation" className="form-label">
                        Digital Signature
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="sign"
                        placeholder="Sign Please"
                        name="signature"
                        value={attendantData.signature}
                        onChange={onChangeHandlers}
                      />
                    </div>
                    <button type="submit" className="btn btn-primary">
                      Submit
                    </button>
                  </form>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-bs-dismiss="modal"
                  >
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div> */}
        <div className="patient row ">
          <div className="col-md-3">
            <button type="button" class="btn btn-primary " data-bs-toggle="modal" data-bs-target="#exampleModal">
              Existing Patient
            </button>
          </div>
          <div className="col-md-3">
            <button
              type="button"
              className="btn btn-primary"
              data-bs-toggle="modal"
              data-bs-target="#exampleModal1"
            >
              Attendant Details
            </button>
          </div>
          <div className="col-md-3">
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#registrationModal">
              New Patient
            </button>
          </div>
          <div className="col-md-3">
            <div className="dropdown">
              <button
                className="btn btn-primary dropdown-toggle"
                type="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Select Doctor
              </button>
              <ul className="dropdown-menu">
                <li className="dropdown-item">Dr. Amit </li>
                <li className="dropdown-item">Dr. Sachin sharma </li>
                <li className="dropdown-item">Dr. Ravindra </li>
                <li className="dropdown-item">Dr. Gaurav </li>
              </ul>
              {/* <ul className="dropdown-menu">
                {doctors.map((doctor) => (
                  <li>doctor.name</li>
                ))}
              </ul> */}
            </div>
          </div>
        </div>
        <div
          className="modal fade"
          id="exampleModal1"
          tabIndex="-1"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h1 className="modal-title fs-5" id="staticBackdropLabel">
                  Please Fill the deatils of Attendant
                </h1>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body">
                <div className="modal-header"></div>
                <form onSubmit={handleSubmit}>
                  <div className="mb-3">
                    <label htmlFor="name" className="form-label">
                      Name / नाम
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="name"
                      placeholder="Enter name"
                      name="attendant_name"
                      value={attendantData.attendant_name}
                      onChange={onChangeHandlers}
                    />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="address" className="form-label">
                      Address / पता
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="address"
                      placeholder="Enter address"
                      name="address"
                      value={attendantData.address}
                      onChange={onChangeHandlers}
                    />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="occupation" className="form-label">
                      Occupation / व्यवसाय
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="occupation"
                      placeholder="Enter occupation"
                      name="occupation"
                      value={attendantData.occupation}
                      onChange={onChangeHandlers}
                    />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="telephone" className="form-label">
                      Telephone / दूरभाष
                    </label>
                    <input
                      type="tel"
                      className="form-control"
                      id="telephone"
                      placeholder="Enter telephone"
                      name="telephone"
                      value={attendantData.telephone}
                      onChange={onChangeHandlers}
                    />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="relation" className="form-label">
                      Relation with Patients (if any) / संबंध
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="relation"
                      placeholder="Enter relation"
                      name="relation_with_patient"
                      value={attendantData.relation_with_patient}
                      onChange={onChangeHandlers}
                    />
                  </div>
                  <div className="mb-3">
                    <label htmlFor="relation" className="form-label">
                      Digital Signature
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="sign"
                      placeholder="Sign Please"
                      name="signature"
                      value={attendantData.signature}
                      onChange={onChangeHandlers}
                    />
                  </div>
                  <button type="submit" className="btn btn-primary">
                    Submit
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xl">
            <div class="modal-content ">
              <div class="modal-header">
                <h1 class="modal-title fs-5" id="exampleModalLabel">Search For Patients</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <div className="row mt-3 Patients">
                  <div className="col-md-6 bg-light rounded">
                    <h4>Search Patients</h4>
                    <hr />
                    <form className='form'>
                      <div className="row">
                        <div className="col-6 mb-3">
                          <label htmlFor="" className="form-label">Search Patients </label>
                          <input
                            type="text"
                            className="col-3 form-control"
                            placeholder="Enter ID / Phone / Email "
                            id="searchInput"
                            name="searchInput"
                          />
                        </div>
                        <div className="col-3 mt-3">
                          <button type="submit" className="btn btn-primary mt-3">Search</button>
                        </div>
                      </div>
                    </form>

                  </div>
                  <div className="col-md-6 bg-light rounded">
                    <h4>Patients Details</h4>
                    <hr />
                    <div className="details">
                      <div className="name">
                        <p> Name : &nbsp; <b>{patientDetails.name}</b></p>
                        <p> Patient ID : &nbsp; <b>{patientDetails.id}</b></p>
                        <p> Phone : &nbsp; <b>{patientDetails.phone}</b></p>
                      </div>
                      <div className="name">
                        <p>Age: &nbsp; <b>{patientDetails.age}</b></p>
                        <p>Address: &nbsp; <b>{patientDetails.address}</b></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <br />
            </div>
          </div>
        </div>
        <div class="modal fade" id="registrationModal" tabIndex="-1" aria-labelledby="registrationModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="registrationModalLabel">Patient Registration Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body ">
                <form className="bg-body-tertiary p-2 rounded-2 ">
                  <div className="row">

                    <div class="mb-3 col-8">
                      <label for="name" class="form-label">Name</label>
                      <input type="text" class="form-control" id="name" placeholder="Enter your name" required />
                    </div>

                    <div class="mb-3 col-4">
                      <label for="age" class="form-label">Age</label>
                      <input type="number" class="form-control" id="age" placeholder="Enter your age" required />
                    </div>
                  </div>
                  <div class="mb-3 col-3 gender">
                    <label class="form-label">Gender</label>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="gender" id="male" value="male" required />
                      <label class="form-check-label" for="male">
                        Male
                      </label>
                    </div>

                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="gender" id="female" value="female" />
                      <label class="form-check-label" for="female">
                        Female
                      </label>
                    </div>
                  </div>
                  <div className="row">

                    <div class="mb-3 col-6">
                      <label for="phone" class="form-label">Phone Number</label>
                      <input type="tel" class="form-control" id="phone" placeholder="Enter your phone number" required />
                    </div>
                    <div class="mb-3 col-6">
                      <label for="email" class="form-label">Email</label>
                      <input type="email" class="form-control" id="email" placeholder="Enter your email" required />
                    </div>
                  </div>
                  <div class="mb-3">
                    <label for="address" class="form-label">Address</label>
                    <input type="text" class="form-control" id="address" placeholder="Enter your address" required />
                  </div>
                  <div class="row mb-3">
                    <div class="col-md-5">
                      <label for="city" class="form-label">City</label>
                      <select class="form-select" id="city" required>
                        <option selected disabled value="">Choose...</option>
                        <option value="city1">City 1</option>
                        <option value="city2">City 2</option>

                      </select>
                    </div>
                    <div class="col-md-4">
                      <label for="state" class="form-label">State</label>
                      <select class="form-select" id="state" required>
                        <option selected disabled value="">Choose...</option>
                        <option value="state1">State 1</option>
                        <option value="state2">State 2</option>

                      </select>
                    </div>
                    <div class="mb-3 col-3">
                      <label for="pincode" class="form-label">Pincode</label>
                      <input type="text" class="form-control" id="pincode" placeholder="Enter your pincode" required />
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <hr className="mt-3
          " />
                  <div className="uhid row gap-1 mt-2">
                    <div className="col-6 border border-info rounded-3 bg-primary-subtle">
                      <h5 className="text d-flex mt-1">UHID : HOSUHID10203394</h5>
                    </div>
                    <div className="col-5">

                      <button type="button" className="btn btn-primary" data-bs-dismiss="modal" aria-label="Close" >
                        Select
                      </button>

                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr className="separator" />
      <div className="form-container">
        <div className="english-content">
          <p className="consent-heading">Consent for Treatment</p>
          <p className="consent-main-para">
            I the undersigned authorize &nbsp;
            <b>
              Dr.
              <span className="doctorName" id="doctorName">
                { }
              </span>
            </b>
            and such another physician/surgeon as may be assigned by them the
            nurses, technician, assistant and other person employed in the
            centre of the treatment of my patient at
            <b> { }</b>. I have been explained that during hospitalization my
            Patient will be administrated drugs either orally or intravenously
            or by any other route as is required and for caring type of
            diagnostic examination, I have been explained the risk associate
            with the drug administration. I.V. Infusion, blood transfusion etc.
            And the future prognosis and cause of disease which my patient is
            having, I voluntarily accept the risk. I have also been explained
            that the facilities better than here may be available at other
            places to deal with emergencies arising out of these situations or
            arising out of disease itself.
          </p>
        </div>
        <div className="ptients-sign">
          <p>Patients Signature / Thumb impression</p>
        </div>
        <div className="hindi-para">
          <p lang="hi" className="hind-consent-main-para">
            <br />
            मैं अधोहस्ताक्षर, डा०. <b>{ }</b> और इस अस्पताल द्वारा निर्धारित किसी
            भी अन्य डाक्टर एंव नर्स, टैक्नीषियन व अन्य सहायक को अपने मरीज का
            ईलाज <b> &nbsp;{ } &nbsp;</b>रूडकी रोड मेरठ में कराने की स्वीकृति
            देता / देती हूँ।
            <br />
            मुझे बता दिया गया है कि ईलाज के दौरान मेरे मरीज को दवाईयाँ, नर्स
            द्वारा मुख में या किसी भी प्रकार से दी जा सकती हैं। मुझे दवाईयों
            द्वारा व खून चढानें द्वारा होने वाली परेषानियों व खतरों से आगाह कर
            दिया गया है। मैं अपनी इच्छा से इन खतरों का सामना करने के लिए तैयार
            हूँ। और हर तरह की जाँच भी कराने के लिए तैयार हूँ। मुझे बता दिया गया
            है कि इस अस्पताल से भी अच्छी सुविधाएँ और जगहों पर भी उपलब्ध हो सकती
            हैं। मुझे सभी जरूरी जानकारी उपलब्ध करा दी गई हैं व मेरे हर प्रष्न का
            उत्तर मिल गया है। मैं पूर्ण रूप से संतुष्ट हूँ।
            <br />
            Details of attendant/Patient admitted by
          </p>
        </div>
        <div className="details-of-attendant">
          <ul>
            <li>
              Name/
              <span lang="hi">
                नाम &nbsp;
                <b className="PatientName">{patientDetails.address}</b>
              </span>
            </li>
            <li>
              Address/
              <span lang="hi">
                पता &nbsp;<b className="Address">{patientDetails.address}</b>
              </span>
            </li>
            <li>
              Occupation/
              <span lang="hi">
                व्यवसाय &nbsp;
                <b className="Occupation"></b>
              </span>
            </li>
            <li>
              Telephone/
              <span lang="hi">
                दूरभाष &nbsp;
                <b className="Phone">{patientDetails.phone}</b>
              </span>
            </li>
            <li>
              Relation with Patients(if any )/
              <span lang="hi">
                संबंध &nbsp;<b className="Relation"></b>
              </span>
            </li>
            <li className="sign">
              <p>Signature / Thumb impression</p>
            </li>
          </ul>
        </div>
      </div>
      <div className="button">
        <button className="btn btn-primary" onClick={handlePrint}>
          Print Form
        </button>
      </div>
    </div>
  );
};

export default Consent_for_treatment;
