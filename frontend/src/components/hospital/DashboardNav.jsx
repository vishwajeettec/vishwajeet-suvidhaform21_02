import React, { useEffect, useState } from "react";
import "./Hospital.css";
import { Link, useLocation, useNavigate } from "react-router-dom";
import axios from "axios";

const DashboardNav = () => {
  const navigate = useNavigate();
  const baseurl = process.env.REACT_APP_BASEURL;
  const [data, setData] = useState({});
  useEffect(() => {
    const id = localStorage.getItem('id');
    const token = localStorage.getItem('token');

    // Make sure ID and token are available
    if (id && token) {
      const fetchData = async () => {
        try {
          // Make API call with the user ID
          const response = await axios.get(`${baseurl}/hospital/${id}`);
          const resdata = response.data;
          if (resdata) {
            setData(resdata)
            // You can update state or perform other actions with the data here
          }
        } catch (error) {
          console.error('API call failed:', error);
        }
      };

      fetchData();
    } else {
      console.error('ID or token not available in local storage');
    }
  }, []); // Remove the empty dependency array to trigger the effect after every render

  // // Access the user data from the location state
  // const userData = location.state ? location.state.user : null;
  // console.log(userData);
  // const navigate = useNavigate();
  // useEffect(() => {
  //   const storedPatientDetails = JSON.parse(
  //     localStorage.getItem("patientDetails")
  //   );
  // }, []);

  const handleLogout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("patientDetails"); // Corrected the removal of localStorage item
    navigate("/login");
  };
  const [sidebarOpen, setSidebarOpen] = useState(false);

  const toggleSidebar = () => {
    setSidebarOpen(!sidebarOpen);
  };
  return (

    <div className="dashboard">

      <div className="container-xxl position-relative bg-white d-flex p-0 ">
        {/* Sidebar Start */}
        <div className="sidebar pe-4 pb-3">
          <nav className="navbar bg-light navbar-light">
            <Link to="" className="navbar-brand mx-4 mb-3">
              <h3 className="text-primary">
                <i className="fa fa-hashtag me-2"></i>Suvidha Form
              </h3>
            </Link>
            <div className="d-flex align-items-center ms-4 mb-4">
              <div className="position-relative">
              {data && data.hospital && (
                   
                <img
                  className="rounded-circle"

                  src={`http://localhost:5000/images/` + data.hospital.logo} // Assuming you have a variable named `logo` with the image URL
                  alt=""
                  style={{ width: "40px", height: "40px" }}
                />
               )}
               {!data && <p>Loading...</p>}
                <div className="bg-success rounded-circle border border-2 border-white position-absolute end-0 bottom-0 p-1"></div>
              </div>
              <div className="ms-3">
                <h6 className="mb-0 text-primary">
                  {/* <b>{data?.hospital?.name}</b> */}
                  {data && data.hospital && (
                   
                      <b>{data.hospital.name}</b>
                    
                  )}
                  {!data && <p>Loading...</p>}
                </h6>
              </div>
            </div>
            <div className="navbar-nav w-100">
              {/* <Link to="/hospital/dashboard" className="nav-item nav-link">
                <i className="fa fa-tachometer-alt me-2"></i>Dashboard
              </Link> */}
              <Link to="/hospital/formManager" className="nav-item nav-link">
                <i className="fa fa-keyboard me-2"></i>Forms
              </Link>

              <Link to="/hospital/patient" className="nav-item nav-link">
                <i className="fa fa-th me-2"></i>New Patients
              </Link>

            </div>
          </nav>
          {/* <button className="btn btn-danger" onClick={logout}> Logout</button> */}
        </div>
        {/* Sidebar End */}

        {/* Content Start */}
        <div className="container-fluid">
          {/* Navbar Start */}
          <nav className="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0">
            <a href="index.html" className="navbar-brand d-flex d-lg-none me-4">
              <h2 className="text-primary mb-0">
                <i className="fa fa-hashtag"></i>
              </h2>
            </a>
            <a href="#" className="sidebar-toggler flex-shrink-0">
              <i className="fa fa-bars"></i>
            </a>
            <form className="d-none d-md-flex ms-4">
              <input
                className="form-control border-0"
                type="search"
                placeholder="Search"
              />
            </form>
            <div className="navbar-nav align-items-center ms-auto">
              <div className="nav-item dropdown">
                <a
                  href="#"
                  className="nav-link dropdown-toggle"
                  data-bs-toggle="dropdown"
                >
                  <i className="fa fa-envelope me-lg-2"></i>
                  <span className="d-none d-lg-inline-flex">Message</span>
                </a>
                <div className="dropdown-menu dropdown-menu-end bg-light border-0 rounded-0 rounded-bottom m-0">
                  <a href="#" className="dropdown-item">
                    <div className="d-flex align-items-center">
                      <img
                        className="rounded-circle"
                        src="../assets/dashboard/img/user.jpg" // Assuming you have a variable named `logo` with the image URL
                        alt=""
                        style={{ width: "40px", height: "40px" }}
                      />
                      <div className="ms-2">
                        <h6 className="fw-normal mb-0">
                          Jhon send you a message
                        </h6>
                        <small>15 minutes ago</small>
                      </div>
                    </div>
                  </a>
                  <hr className="dropdown-divider" />
                  <a href="#" className="dropdown-item">
                    <div className="d-flex align-items-center">
                      <img
                        className="rounded-circle"
                        src="./assets/dashboard/img/user.jpg" // Assuming you have a variable named `logo` with the image URL
                        alt=""
                        style={{ width: "40px", height: "40px" }}
                      />
                      <div className="ms-2">
                        <h6 className="fw-normal mb-0">
                          Jhon send you a message
                        </h6>
                        <small>15 minutes ago</small>
                      </div>
                    </div>
                  </a>
                  <hr className="dropdown-divider" />
                  <a href="#" className="dropdown-item text-center">
                    See all message
                  </a>
                </div>
              </div>
              <div className="nav-item dropdown">
                <a
                  href="#"
                  className="nav-link dropdown-toggle"
                  data-bs-toggle="dropdown"
                >
                  {data && data.hospital && (
                   
                   <img
                     className="rounded-circle"
   
                     src={`http://localhost:3001/images/` + data.hospital.logo} // Assuming you have a variable named `logo` with the image URL
                     alt=""
                     style={{ width: "40px", height: "40px" }}
                   />
                  )}
                  {!data && <p>Loading...</p>}
                  <span className="d-none d-lg-inline-flex">
                  {data && data.hospital && (
                   
                   <b>{data.hospital.email}</b>
                 
               )}
               {!data && <p>Loading...</p>}
                  </span>
                </a>
                <div className="dropdown-menu dropdown-menu-end bg-light border-0 rounded-0 rounded-bottom m-0">
                  <Link href="#" className="dropdown-item">
                    My Profile
                  </Link>
                  <Link href="#" className="dropdown-item">
                    Settings
                  </Link>
                  <button
                    onClick={handleLogout}
                    className="dropdown-item btn btn-danger"
                  >
                    Log Out
                  </button>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  );
};
export default DashboardNav;
